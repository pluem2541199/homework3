const express = require('express');
const ejs = require('ejs');
const path    = require("path");

let bodyParser = require('body-parser');
let jsonParser = bodyParser.json();
let urlencodedParser = bodyParser.urlencoded({ extended: true });

const app = express();

const port = 3000;
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');


app.get('/', (req, res) => {
    res.render('home');
})

app.get('/hnoe', (req, res) => {
    res.render('home');
})


app.get('/login', (req, res) => {
    res.render('login');
})

app.get('/register', (req, res) => {
    res.render('register');
})

app.get('/store', (req, res) => {
    res.render('store');
})

app.listen(port,function() {
  console.log('server started '+ port )
})
